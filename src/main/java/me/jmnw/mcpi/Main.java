package me.jmnw.mcpi;

import me.jmnw.mcpi.pack.PackBuilder;
import me.jmnw.mcpi.pack.PackInstaller;

import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) {
        if (args.length == 0) {
            PackInstaller installer = new PackInstaller(Paths.get("packInfo.json"));
            installer.installPack();
        } else if (args[0].equals("build")) {
            new PackBuilder().build();
        } else {
            PackInstaller installer = null;
            if (args.length == 1 && args[0].equals("install"))
                installer = new PackInstaller(Paths.get("packInfo.json"));
            else if (args.length == 2 && args[0].equals("install"))
                installer = new PackInstaller(Paths.get(args[1]));

            if (installer != null) installer.installPack();
            else
                System.err.println("Usage:\r\n\ttool\t\t\t\t\t(installs default pack)\r\n\ttool build\t\t\t\t(builds a new pack)\r\n\ttool install [packInfo.json file]\t(installs specified pack)");
        }
    }
}
