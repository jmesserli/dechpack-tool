package me.jmnw.mcpi.pack;

import com.google.gson.Gson;
import me.jmnw.mcpi.minecraft.MinecraftJarDownloader;
import me.jmnw.mcpi.minecraft.MinecraftLauncherDownloader;
import me.jmnw.mcpi.model.PackInfoModel;
import me.jmnw.mcpi.net.FileDownloader;
import me.jmnw.mcpi.util.CopyUtils;
import me.jmnw.mcpi.util.FileUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class PackInstaller {

    private Path packInfoPath;
    private PackInfoModel packInfo;

    public PackInstaller(Path packInfoPath) {
        this.packInfoPath = packInfoPath;
        String lines = FileUtils.readAllLines(packInfoPath);
        packInfo = new Gson().fromJson(lines, PackInfoModel.class);
    }

    public void installPack() {
        Path downloadDirectory = Paths.get("installer-download-cache");
        Path launcherDownloadFile = Paths.get(downloadDirectory.toString(), "Minecraft.jar");
        Path jarDownloadFile = Paths.get(downloadDirectory.toString(), String.format("%s.jar", packInfo.getMinecraftVersion()));
        Path sourceModsDirectory = Paths.get(packInfo.getPackLocations().getSourceModsDirectory());
        Path sourceConfigDirectory = Paths.get(packInfo.getPackLocations().getSourceConfigDirectory());
        Path installDirectory = Paths.get(get("Full install path"));
        Path launcherStartScript = Paths.get(installDirectory.toString(), "startPack.cmd");
        Path launcherJar = Paths.get(installDirectory.toString(), "Minecraft.jar");
        Path workDir = Paths.get(installDirectory.toString(), packInfo.getPackLocations().getRelativeInstallWorkingDirectory());
        Path mods = Paths.get(workDir.toString(), "mods");
        Path configs = Paths.get(workDir.toString(), "config");
        Path versions = Paths.get(workDir.toString(), "versions");
        Path launcherProfiles = Paths.get(workDir.toString(), "launcher_profiles.json");
        Path minecraftVersion = Paths.get(versions.toString(), packInfo.getMinecraftVersion());
        Path minecraftJar = Paths.get(minecraftVersion.toString(), String.format("%s.jar", packInfo.getMinecraftVersion()));
        Path minecraftVersionJson = Paths.get(minecraftVersion.toString(), String.format("%s.json", packInfo.getMinecraftVersion()));
        Path forgeVersion = Paths.get(versions.toString(), packInfo.getForgeVersion());
        Path forgeVersionJson = Paths.get(forgeVersion.toString(), String.format("%s.json", packInfo.getForgeVersion()));

        FileDownloader launcherDownloader = null;
        FileDownloader jarDownloader = null;

        try {
            Files.createDirectories(downloadDirectory);

            launcherDownloader = new MinecraftLauncherDownloader(launcherDownloadFile);
            jarDownloader = new MinecraftJarDownloader(packInfo.getMinecraftVersion(), jarDownloadFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Start the downloading threads
        System.out.println("Downloading required files...");
        assert launcherDownloader != null && jarDownloader != null;
        launcherDownloader.start();
        jarDownloader.start();

        // Wait for download completion
        while (!launcherDownloader.isFinished() || !jarDownloader.isFinished()) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Downloads completed.");

        System.out.println("Setting up install directories...");
        try {
            FileUtils.deleteDirectory(installDirectory);
            Files.createDirectories(mods);
            Files.createDirectories(configs);
            Files.createDirectories(versions);
            Files.createDirectories(minecraftVersion);
            Files.createDirectories(forgeVersion);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Created (5) directories.");
        System.out.println("Copying mods, configs and minecraft...");

        CopyUtils.copyDirectory(sourceModsDirectory, mods);
        CopyUtils.copyDirectory(sourceConfigDirectory, configs);
        CopyUtils.copyFile(launcherDownloadFile, launcherJar);
        CopyUtils.copyFile(jarDownloadFile, minecraftJar);

        System.out.println("Copied mods, configs and minecraft.");
        System.out.println("Setting up environment...");

        CopyUtils.copyResource("forge_version.json", forgeVersionJson, packInfo);
        CopyUtils.copyResource("minecraft_version.json", minecraftVersionJson, packInfo);
        CopyUtils.copyResource("launcher_profiles.json", launcherProfiles, packInfo);
        CopyUtils.copyResource("startPack.cmd", launcherStartScript, packInfo);

        System.out.printf("Setup finished. Enjoy playing %s!%n", packInfo.getPackName());
    }

    private String get(String prefix) {
        System.out.printf("%s: ", prefix);
        return new Scanner(System.in).nextLine();
    }
}
