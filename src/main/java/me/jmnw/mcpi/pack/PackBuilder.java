package me.jmnw.mcpi.pack;

import com.google.gson.Gson;
import me.jmnw.mcpi.model.PackInfoModel;
import me.jmnw.mcpi.model.PackLocationModel;
import me.jmnw.mcpi.util.FileUtils;

import java.nio.file.Paths;
import java.util.Scanner;

public class PackBuilder {

    private PackInfoModel model = new PackInfoModel();

    public void build() {
        PackLocationModel locationModel = new PackLocationModel();

        model.setPackName(get("Pack name"));
        model.setLauncherProfileName(get("Launcher profile name"));
        model.setMinecraftVersion(get("Minecraft version"));
        model.setForgeVersion(get("MinecraftForge version"));

        locationModel.setSourceModsDirectory(get("Source mods directory"));
        locationModel.setSourceConfigDirectory(get("Source config directory"));
        locationModel.setRelativeInstallWorkingDirectory(get("Install working directory name"));

        model.setPackLocations(locationModel);

        String filename = String.format("%s.json", get("JSON filename"));
        String jsonString = new Gson().toJson(model);
        FileUtils.writeString(jsonString, Paths.get(filename));
    }

    private String get(String prefix) {
        System.out.printf("%s: ", prefix);
        return new Scanner(System.in).nextLine();
    }
}
