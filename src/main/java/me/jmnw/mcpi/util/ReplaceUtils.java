package me.jmnw.mcpi.util;

import java.util.HashMap;

public class ReplaceUtils {

    public static String replaceMultiple(String input, HashMap<String, String> replacements) {
        for (String toReplace : replacements.keySet())
            input = input.replaceAll(String.format("@%s@", toReplace), replacements.get(toReplace));

        return input;
    }

}
