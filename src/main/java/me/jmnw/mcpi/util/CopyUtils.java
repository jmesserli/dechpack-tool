package me.jmnw.mcpi.util;

import me.jmnw.mcpi.model.PackInfoModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;

public class CopyUtils {

    public static void copyResource(String resourceName, Path target, PackInfoModel packInfo) {
        StringBuilder sb = new StringBuilder();

        try (InputStream is = CopyUtils.class.getResourceAsStream(String.format("/%s", resourceName))) {
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = br.readLine()) != null) sb.append(line).append("\n");
        } catch (IOException e) {
            e.printStackTrace();
        }

        String resourceContent = sb.toString();
        resourceContent = packInfo.performReplacements(resourceContent);

        FileUtils.writeString(resourceContent, target);
        System.out.printf("\tCOPIED %s -> %s%n", resourceName, target);
    }

    public static void copyFile(Path source, Path target) {
        try {
            Files.copy(source, target);
            System.out.printf("\tCOPIED %s -> %s%n", source, target);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void copyDirectory(Path source, Path target) {
        FileVisitor<Path> treeCopier = new TreeCopyVisitor(source, target);
        try {
            if (Files.notExists(target)) Files.createDirectories(target);
            Files.walkFileTree(source, treeCopier);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static class TreeCopyVisitor implements FileVisitor<Path> {
        private final Path source;
        private final Path target;

        public TreeCopyVisitor(Path source, Path target) {
            this.source = source;
            this.target = target;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
            // from target, resolve the relative path from source to the current directory, yielding the target directory
            Path targetDir = target.resolve(source.relativize(dir));
            Files.createDirectories(targetDir);
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            copyFile(file, target.resolve(source.relativize(file)));
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
            return FileVisitResult.CONTINUE;
        }
    }

}
