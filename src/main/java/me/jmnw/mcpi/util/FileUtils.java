package me.jmnw.mcpi.util;


import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Optional;

public class FileUtils {

    public static String readAllLines(Path file) {
        Optional<String> lines = null;
        try {
            lines = Files.readAllLines(file)
                    .stream()
                    .reduce((String all, String element) -> String.format("%s\n%s", all, element));

            return lines.get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void writeString(String output, Path file) {
        try {
            Files.write(file, output.getBytes("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deleteDirectory(Path directory) {
        FileVisitor<Path> treeDeleter = new DeleteTreeVisitor();

        try {
            Files.walkFileTree(directory, treeDeleter);
            Files.deleteIfExists(directory);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static class DeleteTreeVisitor implements FileVisitor<Path> {

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            Files.delete(file);
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
            Files.delete(dir);
            return FileVisitResult.CONTINUE;
        }
    }

}
