package me.jmnw.mcpi.net;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public class FileDownloader extends Thread {
    private URL fileUrl;
    private Path downloadPath;

    private boolean finished = false;

    public FileDownloader(String fileUrl, Path downloadPath) throws MalformedURLException {
        this(new URL(fileUrl), downloadPath);
    }

    public FileDownloader(URL fileUrl, Path downloadPath) {
        this.fileUrl = fileUrl;
        this.downloadPath = downloadPath;
    }

    @Override
    public void run() {
        if (finished) throw new IllegalStateException("Download already completed");

        if (Files.exists(downloadPath)) {
            System.out.printf("\tSKIPPING %s (cached)%n", downloadPath);
            finished = true;
            return;
        }

        try {
            Files.copy(fileUrl.openStream(), downloadPath, StandardCopyOption.REPLACE_EXISTING);
            System.out.printf("\tDOWNLOADED %s (%s Byte)%n", downloadPath, Files.size(downloadPath));
        } catch (IOException e) {
            e.printStackTrace();
        }

        finished = true;
    }

    //region Getters and setters
    public URL getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(URL fileUrl) {
        this.fileUrl = fileUrl;
    }

    public Path getDownloadPath() {
        return downloadPath;
    }

    public void setDownloadPath(Path downloadPath) {
        this.downloadPath = downloadPath;
    }

    public boolean isFinished() {
        return finished;
    }
    //endregion
}
