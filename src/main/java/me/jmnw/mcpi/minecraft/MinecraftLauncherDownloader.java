package me.jmnw.mcpi.minecraft;

import me.jmnw.mcpi.net.FileDownloader;

import java.net.MalformedURLException;
import java.nio.file.Path;

public class MinecraftLauncherDownloader extends FileDownloader {
    public static final String minecraftLauncherLocation = "https://s3.amazonaws.com/Minecraft.Download/launcher/Minecraft.jar";

    public MinecraftLauncherDownloader(Path downloadPath) throws MalformedURLException {
        super(minecraftLauncherLocation, downloadPath);
    }
}
