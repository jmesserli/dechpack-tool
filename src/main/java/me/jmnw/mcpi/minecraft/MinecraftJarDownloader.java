package me.jmnw.mcpi.minecraft;

import me.jmnw.mcpi.net.FileDownloader;

import java.net.MalformedURLException;
import java.nio.file.Path;

public class MinecraftJarDownloader extends FileDownloader {
    public static final String minecraftJarLocation = "https://s3.amazonaws.com/Minecraft.Download/versions/%s/%s.jar";

    public MinecraftJarDownloader(String version, Path downloadPath) throws MalformedURLException {
        super(String.format(minecraftJarLocation, version, version), downloadPath);
    }
}
