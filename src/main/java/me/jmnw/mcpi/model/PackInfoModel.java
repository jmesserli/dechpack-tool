package me.jmnw.mcpi.model;

import com.google.gson.annotations.SerializedName;
import me.jmnw.mcpi.util.ReplaceUtils;

import java.util.HashMap;

/**
 * Model class for "packInfo.json", holds all the pack data
 */
public class PackInfoModel {
    @SerializedName("pack_name")
    private String packName;
    @SerializedName("launch_profile_name")
    private String launcherProfileName;
    @SerializedName("mc_version")
    private String minecraftVersion;
    @SerializedName("mc_forge_version")
    private String forgeVersion;
    @SerializedName("pack_dirs")
    private PackLocationModel packLocations;

    public String performReplacements(String input) {
        return ReplaceUtils.replaceMultiple(input, getReplacementsMap());
    }

    public HashMap<String, String> getReplacementsMap() {
        HashMap<String, String> replacements = new HashMap<>();

        replacements.put("PACK_NAME", packName);
        replacements.put("PROFILE_NAME", launcherProfileName);
        replacements.put("MC_VERSION", minecraftVersion);
        replacements.put("FORGE_VERSION", forgeVersion);
        replacements.put("REL_WORK_DIR", packLocations.getRelativeInstallWorkingDirectory());
        replacements.put("SRC_CONF_DIR", packLocations.getSourceConfigDirectory());
        replacements.put("SRC_MODS_DIR", packLocations.getSourceModsDirectory());

        return replacements;
    }

    //region Getters and setters

    public String getPackName() {
        return packName;
    }

    public void setPackName(String packName) {
        this.packName = packName;
    }

    public String getLauncherProfileName() {
        return launcherProfileName;
    }

    public void setLauncherProfileName(String launcherProfileName) {
        this.launcherProfileName = launcherProfileName;
    }

    public String getMinecraftVersion() {
        return minecraftVersion;
    }

    public void setMinecraftVersion(String minecraftVersion) {
        this.minecraftVersion = minecraftVersion;
    }

    public String getForgeVersion() {
        return forgeVersion;
    }

    public void setForgeVersion(String forgeVersion) {
        this.forgeVersion = forgeVersion;
    }

    public PackLocationModel getPackLocations() {
        return packLocations;
    }

    public void setPackLocations(PackLocationModel packLocations) {
        this.packLocations = packLocations;
    }

    //endregion
}
