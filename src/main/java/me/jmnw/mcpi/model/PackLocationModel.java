package me.jmnw.mcpi.model;

import com.google.gson.annotations.SerializedName;

public class PackLocationModel {
    @SerializedName("src_mods_dir")
    private String sourceModsDirectory;
    @SerializedName("src_conf_dir")
    private String sourceConfigDirectory;
    @SerializedName("rel_install_workdir")
    private String relativeInstallWorkingDirectory;

    //region Getters and setters
    public String getSourceModsDirectory() {
        return sourceModsDirectory;
    }

    public void setSourceModsDirectory(String sourceModsDirectory) {
        this.sourceModsDirectory = sourceModsDirectory;
    }

    public String getSourceConfigDirectory() {
        return sourceConfigDirectory;
    }

    public void setSourceConfigDirectory(String sourceConfigDirectory) {
        this.sourceConfigDirectory = sourceConfigDirectory;
    }

    public String getRelativeInstallWorkingDirectory() {
        return relativeInstallWorkingDirectory;
    }

    public void setRelativeInstallWorkingDirectory(String relativeInstallWorkingDirectory) {
        this.relativeInstallWorkingDirectory = relativeInstallWorkingDirectory;
    }
    //endregion
}
